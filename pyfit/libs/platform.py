'''This file contains some functions and variables that have to do with the operating system, assets location and so on.'''

######### Others #########
import os
import platform
import pwd

def get_icon(activitytype):
    dictionary = {  'Walking':              'media/walking.svg',
                    'Running':              'media/running.svg',
                    'Biking':               'media/cycling.svg',
                    'Dancing':              'media/dancing.svg',
                    'Sleep':                'media/sleeping.svg',
                    'Biking.stationary':    'media/staticcycling.svg',
                }
    iconpath = dictionary.get(activitytype)
    if iconpath == None:
        iconpath = 'media/error.svg'
    return iconpath

def get_filepath(file):
    if platform.system() == 'Linux':
        return os.path.join(os.path.expanduser('~') + '/.config/PyFit/', file)

def get_userpicture():
    if platform.system() == 'Linux':
        face = os.path.join('/var/lib/AccountsService/icons/', pwd.getpwuid(os.getuid())[0])
        if not os.path.exists(face):
            face = '/usr/share/pixmaps/faces/surfer.jpg'
        return face

def get_username():
    if platform.system() == 'Linux':
        return pwd.getpwuid(os.getuid())[4].split(',')[0]

pyfiticon = 'media/icon.svg'
settingspath = get_filepath('settings.json')
databasepath = get_filepath('database.db')
gbdatabasepath = get_filepath('gadgetbridge.db')
userpicture = get_userpicture()
username = get_username()
gtkversion = '3.0'
