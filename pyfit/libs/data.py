'''This file specifies a data model for the user's information.'''

##### PyFit modules. #####
from libs.platform import databasepath, settingspath
###### Time and date #####
import datetime as dt
######### Others #########
import sqlite3
import os
import json
import gettext
_g = gettext.gettext

Activitytypes = ['Walking', 'Running', 'Biking', 'Biking.stationary', 'Dancing', 'Swimming']

class Activity:
    """
    This class represents a generic physical activity
    performed by an individual, ej. walking or biking
    (including sleeping).

    It is a very simple model of a physical activity,
    that is characterized by the following attributes:
    - Type: the kind of activity being performed (sleep,
        running, biking, walking, ...) (of string type).
    - StartTime: the time when the activity started (as a datetime object).
    - EndTime: the time when the activity stopped (as a datetime object).
    - Distance: The distance covered when performing
        the activity (in meters).
    - Calories: the amount of calories (in kcal) burned
        while performing the activity.
    - Track: This is a dictionary that keeps track of the
        path followed for activities that involve motion
        if such information is available.

    In addition, each activity has an associated ID that,
    should be unique and identifies each activity. A good
    recommendation to use as ID could be the starting time,
    as people are not expected to perform two physical activities,
    simultaneously.
    """
    databasetypes   = {'id': 'text', 'type': 'text', 'starttime': 'text', 'endtime': 'text',
                       'distance': 'real', 'calories': 'real', 'steps': 'real',
                       'tracktable': 'text', 'deviceid': 'text'}
    databasetypes_track   = {'time': 'text', 'distance': 'real', 'latitude': 'real', 'longitude': 'real',
                       'altitude': 'real'}
    database_to_object = {'id': 'ID', 'type': 'Type', 'starttime': 'StartTime', 'endtime': 'EndTime', 'distance': 'Distance', 'calories': 'Calories', 'steps': 'Steps', 'tracktable': 'Track', 'deviceid': 'DeviceID'}
    database_track_to_object = {'time': 'Time', 'distance': 'Distance', 'latitude': 'Latitude', 'longitude': 'Longitude', 'altitude': 'Altitude'}

    def __repr__(self):
        return '<' + type(self).__module__ + '.' + type(self).__name__ + ' object>' + ' id: ' + str(self.ID) + ' type: ' + str(self.Type)

    def __init__(self, ID, Type=None, StartTime=None, EndTime=None, Distance=None, Calories=None, Steps=None, Track=[], DeviceID='PyFit'):
        # Main activity information.
        self.Type = Type
        self.StartTime = StartTime
        self.EndTime = EndTime
        self.Distance = Distance
        self.Calories = Calories
        # TODO: Currently there is no way to view the tracks.
        self.Track = Track
        self.Steps = Steps

        # For storage purposes.
        self.ID = ID
        self.DeviceID = DeviceID

    @property
    def Duration(self):
        return self.EndTime-self.StartTime

    @Duration.setter
    def Duration(self, value):
        raise Exception(_g('This property cannot be directly set.'))

    @property
    def databaseheaders(self):
        return ( key for key, item in self.databasetypes.items() )

    @property
    def databaseheaders_track(self):
        return ( key for key, item in self.databasetypes_track.items() )

    # Writing, deleting and extracting from database.

    def writetodatabase(self, conn):
        """
        Insert a new activity in the database.
        Requires an existing connection to the
        database to be passed.

        Such a connection can be created with
        the function 'create_connection'.
        """
        #TODO: This can be adapted to take the headers from databaseheaders.
        query = ''' INSERT INTO activities(id, type, starttime, endtime, distance, calories, steps, tracktable, deviceid) VALUES (?,?,?,?,?,?,?,?,?)'''
        serializedactivity = self.serialize()
        cur = conn.cursor()
        cur.execute(query, serializedactivity)
        #TODO: The reference to the track field should be dynamic.
        # otherwise when fields are added or removed to the class, this will fail.
        if serializedactivity[-2] != 'None':
            self.writetracktodatabase(conn)

    def writetracktodatabase(self, conn):
        """
        Sports activities may include a track
        that represents the path followed by the
        person. The track may have an unlimited
        amount of 'trackpoints', which are the places
        and times of the path that were tracked.
        Because of this, to store this track it is
        necessary to create a SQL table just for it.

        This function inserts the corresponding track
        into the database whenever it exists. A connection
        to the database needs to be passed.
        """

        #TODO: the tracks should be stored in a file instead. Otherwise the database fills with hundreds of tables.

        if not self.Track:
            raise Exception(_g('Attempted to write the track of an activity that does not contain a track.'))
            return None

        # TODO: This can be adapted to take the headers from databaseheaders_track.
        sql_create_track_table = '''CREATE TABLE IF NOT EXISTS track_{} (
                                            time text PRIMARY KEY,
                                            distance real,
                                            latitude real,
                                            longitude real,
                                            altitude real
                                            ); '''.format(self.ID.replace('-','_').replace(':','_').replace('.','_').replace('+','_'))
        cur = conn.cursor()
        cur.execute(sql_create_track_table)
        # TODO: This can be adapted to take the headers from databaseheaders_track.
        query = ''' INSERT INTO track_{}(time, distance, latitude, longitude, altitude) VALUES (?,?,?,?,?)'''.format(self.ID.replace('-','_').replace(':','_').replace('.','_').replace('+','_'))
        for trackpoint in self.Track:
            serializedtrackpoint = ( trackpoint['Time'].isoformat(),
                                     trackpoint['Distance'],
                                     trackpoint.get('Latitude'),
                                     trackpoint.get('Longitude'),
                                     trackpoint.get('Altitude')
                                   )
            cur.execute(query, serializedtrackpoint)

    @staticmethod
    def extractfromdatabase(id, conn):
        """
        Just a placeholder that calls the deserialize method.
        The deserialize method can take either a tuple representing
        a serialized activity or just its id. From this information
        it creates and returns an activity object.
        """
        return Activity.deserialize(id, conn)

    def deletefromdatabase(self, conn):
        """
        Deletes an activity (based on its ID) from the database.
        """
        sql = 'DELETE FROM activities WHERE id=?'
        cur = conn.cursor()
        cur.execute("SELECT tracktable FROM activities WHERE id=?", (self.ID,))
        tracktable = cur.fetchone()[0]
        sql_droptable = 'DROP TABLE {}'.format(tracktable)
        cur.execute(sql, (self.ID,))
        if sql_droptable != 'DROP TABLE None':
            cur.execute(sql_droptable)

    # Serialization and deserialization
    # methods to insert into or extract
    # from the database.

    def serialize(self):
        """
        The serialize method produces a tuple representing
        the activity. Each element of such tuple is a column
        to be stored in the database. Tracks are stored
        in separate tables with the appropriate name.
        """

        #TODO: The reference to the fields should be dynamic.
        # otherwise when fields are added or removed to the class, this will fail.
        serializedactivity =  [ self.ID,
                                self.Type,
                                self.StartTime.isoformat(),
                                self.EndTime.isoformat(),
                                self.Distance,
                                self.Calories,
                                self.Steps,
                                str('track_' + self.ID).replace('-','_').replace(':','_').replace('.','_'),
                                self.DeviceID
                                ]
        if not self.Track:
            serializedactivity[-2] = 'None'
        return tuple(serializedactivity)

    @staticmethod
    def deserialize(serializedactivity, conn=None):
        """
        The deserialize method can take either a tuple representing
        a serialized activity or just its id. From this information
        it creates and returns an activity object.
        """

        """If a string is given instead of a serialized activity
        tuple, then extract the tuple from the database."""
        if (type(serializedactivity) == type('')):
            id = serializedactivity
            cur = conn.cursor()
            cur.execute("SELECT * FROM activities WHERE id=?", (id,))
            serializedactivity = cur.fetchall()[0]

        """At this point the serialized activity tuple is available,
        either because it has been provided or extracted from the
        database. Now the activity can be generated from the tuple."""
        database_to_object = Activity.database_to_object
        databaseheaders = [ key for key in Activity.databasetypes ]
        activity_dict = { database_to_object[databaseheaders[i]]: serializedactivity[i] for i in range(0,len(serializedactivity)) }
        # Extrack track from database.
        if activity_dict['Track'] != 'None':
            activity_dict['Track'] = Activity.deserialize_trackfromdatabase(conn, activity_dict['Track'])
        else:
            activity_dict['Track'] = []
        # Convert ISO timestamps to datetime objects
        activity_dict['StartTime'] = dt.datetime.fromisoformat(activity_dict['StartTime'])
        activity_dict['EndTime'] = dt.datetime.fromisoformat(activity_dict['EndTime'])
        activity = Activity(activity_dict['ID'], **{ key: item for key, item in activity_dict.items() if key not in ['ID'] } )
        return activity

    @staticmethod
    def deserialize_trackfromdatabase(conn, table):
        """
        This method reads a track from the database (by
        specifying its table name) and returns a dictionary
        representing the track.
        """
        cur = conn.cursor()
        cur.execute("SELECT * FROM " + table)
        serialized_trackpoints = cur.fetchall()
        track = []
        for serialized_trackpoint in serialized_trackpoints:
            trackpoint = { database_track_to_object[databaseheaders_track[i]]: serialized_trackpoint[i] for i in range(0,len(serialized_trackpoint)) }
            trackpoint['Time'] = dt.datetime.fromisoformat(trackpoint['Time'])
            track.append(trackpoint)
        return track

    # Query the database for multiple activities.

    @staticmethod
    def fetch_by_date(date, end=None, conn=None, type=None):
        """
        Given a date, this method fetchs all the activities of the given type
        from the database. If no type is specified, then it fetchs all
        activities for that particular date. If another date is provided through
        the "end" parameter, then the function selects the activities in the
        range between "date" and "end" instead.
        """
        if not end:
            query = "SELECT id FROM activities WHERE endtime LIKE ?"
        else:
            query = "SELECT id FROM activities WHERE endtime >= ? AND endtime < ?"
        if type:
            query += " AND type LIKE '{}'".format(type)
        cur = conn.cursor()
        if not end:
            cur.execute(query, (date.isoformat() + '%',))
        else:
            cur.execute(query, (dt.datetime(date.year, date.month, \
                                date.day, 0, 0), \
                                dt.datetime(end.year, end.month, \
                                end.day, 0, 0) + dt.timedelta(days=1) ))
        activityids = cur.fetchall()
        activityids = ( id[0] for id in activityids )
        activitylist = [ Activity.extractfromdatabase(id, conn) for id in activityids ]
        return activitylist

class Data:
    """
    This class represents a generic data point,
    such as a heart rate measurement or a weight
    measurement.

    Data points are characterized by the following attributes:
    - Type: the type of data point, it serves as a label
        this can take values such as 'Weight' or 'HeartRate'.
    - Time: the time when the measurement was taken (as a
        datetime object).
    - Value: the value of the datapoint.
    """

    databasetypes   = {'type': 'text', 'time': 'timestamp', 'value': 'real', 'deviceid': 'text'}
    database_to_object = {'type': 'Type', 'time': 'Time', 'value': 'Value', 'deviceid': 'DeviceID'}

    def __init__(self, Type=None, Time=None, Value=None, DeviceID='PyFit'):
        self.Type = Type
        self.Time = Time
        self.Value = Value
        self.DeviceID=DeviceID

    def __repr__(self):
        return '<' + type(self).__module__ + '.' + type(self).__name__ + ' object>' + ' type: ' + str(self.Type) + ' Time: ' + str(self.Time) + ' Value: ' + str(self.Value)

    @property
    def databaseheaders(self):
        return (key for key, item in self.databasetypes.items())

    # Database insertion, extraction and deletion.

    def writetodatabase(self, conn):
        """
        Inserts a new datapoint in the database.
        Requires an existing connection to the
        database to be passed.

        Such a connection can be created with
        the function 'create_connection'.
        """
        #TODO: This can be adapted to take the headers from databaseheaders.
        sql = '''INSERT INTO datapoints(type, time, value, deviceid) VALUES (?,?,?,?)'''
        serialized = self.serialize()
        cur = conn.cursor()
        try:
            cur.execute(sql, serialized)
        except sqlite3.IntegrityError as e:
            if str(e) == 'UNIQUE constraint failed: datapoints.id':
                #TODO: This should be avoided. Something has to be thought of what to do
                # in this situation.
                pass
            else:
                raise sqlite3.IntegrityError(str(e))

    @staticmethod
    def extractfromdatabase(type, time, conn):
        """
        Just a placeholder that calls the deserialize method.
        The deserialize method can take either a tuple representing
        a serialized activity or just its id. From this information
        it creates and returns an activity object.
        """
        id = (type, time)
        return Data.deserialize(id, conn)

    def deletefromdatabase(self, conn):
        """
        Deletes an activity (based on its ID) from the database.
        """
        sql = 'DELETE FROM datapoints WHERE type LIKE ? AND time = ?'
        cur = conn.cursor()
        cur.execute(sql, (self.Type, self.Time))

    # Serialization and deserialization
    # methods to insert into or extract
    # from the database.

    def serialize(self):
        """
        The serialize method produces a tuple representing
        the datapoint. Each element of such tuple is a column
        to be stored in the database.
        """
        #TODO: The reference to the fields should be dynamic.
        # otherwise when fields are added or removed to the class, this will fail.
        serialized =  [ self.Type,
                        self.Time,
                        self.Value,
                        self.DeviceID
                        ]
        return tuple(serialized)

    @staticmethod
    def deserialize(serialized, conn=None):
        """
        The deserialize method can take either a tuple representing
        a serialized datapoint or just its id (which is a tuple
        containing its type and timestamp). From this information
        it creates and returns an activity object.
        """

        """If a type and time (primary keys) instead of a serialized
        datapoint are given as a tuple, then extract the value from
        the database."""
        if type(serialized) == type(tuple([1])) and len(serialized) == 2:
            id = serialized
            cur = conn.cursor()
            cur.execute("SELECT * FROM datapoints WHERE type LIKE ? AND time = ?", (id[0], id[1]))
            serialized = cur.fetchall()
            if type(serialized) == type([]):
                serialized = serialized[0]

        """At this point the serialized datapoint tuple is available,
        either because it has been provided or extracted from the
        database. Now the datapoint can be generated from the tuple."""
        database_to_object = Data.database_to_object
        databaseheaders = [ key for key in Data.databasetypes ]
        datapoint_dict = { database_to_object[databaseheaders[i]]: serialized[i] for i in range(0,len(serialized)) }
        # Restore timestamp class
        datapoint_dict['Time'] = datapoint_dict['Time'].replace(' ', 'T')
        datapoint_dict['Time'] = dt.datetime.fromisoformat(datapoint_dict['Time'])
        datapoint = Data(**datapoint_dict)
        return datapoint

        # Query the database for multiple datapoints.

    @staticmethod
    def fetch_by_date(date, end=None, conn=None, type=None):
        """
        Given a date, this method fetchs all the datapoints of the given type
        from the database. If another date is provided through the "end"
        parameter, then the function selects the activities in the range between
        "date" and "end" instead.
        """
        if not end:
            query = "SELECT type, time FROM datapoints WHERE time LIKE ? AND type LIKE '{}'".format(type)
        else:
            query = "SELECT type, time FROM datapoints WHERE time >= ? AND time < ? AND type LIKE '{}'".format(type)
        cur = conn.cursor()
        if not end:
            cur.execute(query, (date.strftime('%Y-%m-%d') + '%',))
        else:
            cur.execute(query, (dt.datetime(date.year, date.month, \
                                date.day, 0, 0), \
                                dt.datetime(end.year, end.month, \
                                end.day, 0, 0) + dt.timedelta(days=1) ))
        datapointids = cur.fetchall()
        datapointids = ( (type, time) for type, time in datapointids )
        datapointlist = [ Data.extractfromdatabase(*id, conn) for id in datapointids ]
        return datapointlist


#########################
### Activity database ###
#########################

sql_create_activities_table = 'CREATE TABLE IF NOT EXISTS activities ( ' + ','.join(['{} PRIMARY KEY'] + ['{}' for i in
                                        range(0,len(Activity.databasetypes)-1)]) + ' );'
sql_create_activities_table = sql_create_activities_table.format(*[ ' '.join((key, item)) for key, item in Activity.databasetypes.items() ])

sql_create_datapoints_table = 'CREATE TABLE IF NOT EXISTS datapoints ( ' + ','.join(['{}' for i in
                                        range(0,len(Data.databasetypes))]) + ', PRIMARY KEY (type, time) );'
sql_create_datapoints_table = sql_create_datapoints_table.format(*[ ' '.join((key, item)) for key, item in Data.databasetypes.items() ])

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    """
    conn = sqlite3.connect(db_file)
    return conn

def init_database():
    conn = create_connection(databasepath)
    if conn:
        c = conn.cursor()
        c.execute(sql_create_activities_table)
        c.execute(sql_create_datapoints_table)
        conn.close()
    else:
        print(_g("Error! cannot create the database connection."))

def clear_database():
    print(_g('Clearing database...'))
    conn = create_connection(databasepath)
    c = conn.cursor()
    c.execute("select name from sqlite_master where type = 'table'" )
    tablelist = [ tuple[0] for tuple in c.fetchall() ]
    for table in tablelist:
        c.execute('DROP TABLE {}'.format(table))
    conn.commit()
    conn.close()

#########################
##### Settings area #####
#########################

defaultsettings = {'gbmethod': 'readfile',
                    'gbpath': None,
                    'scriptpath': None,
                    'gbremovefile': False,
                    'gbimportmode': 'Refresh',
                    'distancegoal': None,
                    'distancegoalenabled': False,
                    'stepsgoal': None,
                    'stepsgoalenabled': False,
                    'minutesgoal': 60,
                    'minutesgoalenabled': True,
                    'goaltimeframe': 'Daily'}

def init_settings():
    exists = os.path.isfile(settingspath)
    if exists:
        settings = read_settings()
    with open(settingspath, 'w') as file:
        if exists:
            for key in defaultsettings:
                if key not in settings:
                    settings[key] = defaultsettings[key]
            json.dump(settings, file)
        else:
            json.dump(defaultsettings, file)

def read_settings(*args):
    with open(settingspath, 'r') as file:
        settings = json.load(file)
    if len(args) > 0:
        result = { key: item for key, item in settings.items() if key in args }
        if len(result) == 1:
            result = result[0]
        elif len(result) == 0:
            result = None
    else:
        result = settings
    return result

def write_settings(dictionary):
    with open(settingspath, 'r') as file:
        settings = json.load(file)
    for key, value in dictionary.items():
        settings[key] = value
    with open(settingspath, 'w') as file:
        json.dump(settings, file)
    return True
