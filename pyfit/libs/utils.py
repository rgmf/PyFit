'''This file just contains auxiliary functions that repeat through the code.'''

###### Time and date #####
import datetime as dt
import calendar as cal
import time

def attachlocaltimezone(datetime):
    timestamp = datetime.timestamp()
    dst = time.localtime(timestamp).tm_isdst
    tz = dt.timezone(dt.timedelta(seconds=-time.timezone) + dst * dt.timedelta(seconds=3600))
    return datetime.replace(tzinfo=tz)

