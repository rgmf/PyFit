'''This file takes care of managing the settings menu of the app.'''

##### PyFit modules. #####
from libs import data

####### PyGobject ########
import gi
from libs.platform import gtkversion
gi.require_version('Gtk', gtkversion)
from gi.repository import Gtk
######### Others #########
import gettext
_g = gettext.gettext

class SettingsScreen(Gtk.Dialog):
    """
    This class represents the settings screen of  It is
    composed just of a sidebar object and a stack object,
    inserted into a grid. By itself it does not allow to set any
    settings. Indeed the stack is a container for the class
    "SettingsSection" (and its subclasses) defined after this one.

    When initialized, the SettingsScreen will store the program's
    settings in the property self.settings. When the dialog is destroyed,
    it calls every "SettingsSection" to update self.settings with the new
    settings selected by the user and then writes it down to a json file
    in the application's settings directory.
    """

    #### Properties ##############

    @property
    def settings(self):
        return data.read_settings()

    @settings.setter
    def settings(self, value):
        data.write_settings(value)

    #### Initialization ##########

    def __init__(self, toplevel):
        super().__init__(_g("Settings"), toplevel)
        self.connect("delete-event", self.quit)

        # Set settings window characteristics.
        self.set_modal(True)
        self.set_default_size(650, 400)
        self.set_resizable(True)

        # Create the window layout.
        self.maingrid = Gtk.Grid.new()
        self.sidebar = Gtk.StackSidebar()
        self.stack = Gtk.Stack()
        self.sidebar.set_stack(self.stack)
        self.maingrid.attach(self.sidebar, 0, 0, 1, 1)
        self.maingrid.attach(self.stack, 2, 0, 1, 1)
        self.get_content_area().add(self.maingrid)

        # Now attach the sections.
        sections = [(DeviceSync(), 'DeviceSync', _g('GadgetBridge connection')),
                    (Goals(), 'Goals', _g('Goals'))]
        for item, name, description in sections:
            self.stack.add_titled(item, name, description)

        # Finally, update the GUI of each section to reflect the current settings and show the window.
        for child in self.stack.get_children():
            child.update_gui()
        self.show_all()

    #### Action handlers ###

    # This function retrieves all the changes made by the user and saves them to the settings file.
    # In order to do that, it calls a function on each individual section that returns the settings
    # that the section is responsible for.
    def quit(self, *args):
        settings = {}
        for child in self.stack.get_children():
            settings.update(child.settings)
        self.settings = settings

class SettingsSection(Gtk.ScrolledWindow):
    """This class represents a tab that can be selected in the settings menu. Note how
    it inherits from a scrolled window as a means to adapt to small screen sizes
    (as well as warping most text)."""

    #### Properties ##############

    iconsize = 2

    #### Initialization ##########

    def __init__(self):
        super().__init__(self, hadjustment=None, vadjustment=None)

        # Layout of the section.
        self.set_hexpand(True); self.set_vexpand(True)
        self.box = Gtk.VBox(spacing=35)
        self.box.set_margin_top(30)
        self.box.set_margin_bottom(30)
        self.box.set_margin_right(30)
        self.box.set_margin_left(30)
        self.add(self.box)

        # Spawn each subsection.
        for key in self.subsections:
            name = self.subsections[key]
            self.subsections[key] = {}
            self.subsections[key]['box'] = self.createsubsection(name)
            self.box.pack_start(self.subsections[key]['box'], expand=False, fill=False, padding=0)

    #### Auxiliary functions #####

    def createsubsection(self, title):
        label = Gtk.Label()
        label.set_markup('<big><b>{}</b></big>'.format(title))
        label.set_xalign(0)
        label.set_line_wrap(True)
        sectionbox = Gtk.VBox(spacing=5)
        sectionbox.pack_start(label, expand=True, fill=True, padding=0)
        return sectionbox

    def creategroup(self, type, function, descriptions):
        '''Creates a group of radio buttons or checkboxes. It returns a lit of Gtk boxes
        containing:
        - An "item box" that has an attribute "item" referencing the actual radio button or checkbox.
        - A "content box" that contains the item's description. Further content can be added to it in order to
          expand it's functionality.
        '''

        prev = None
        itemboxes = []
        for i in range(0, len(descriptions)):
            # The button itself.
            if type == 'Radio':
                item = Gtk.RadioButton.new_from_widget(prev)
                prev = item
            elif type == 'Checkbox':
                item = Gtk.CheckButton()
            if function:
                item.connect({'Radio': 'toggle', 'Checkbox': 'activate'}[type], function, i)
            item.set_valign(Gtk.Align(1))
            item.set_halign(Gtk.Align(1))
            # Dummy box that contains the button or box have a margin before any text.
            itembox = Gtk.Box()
            itembox.set_size_request(25, 0)
            itembox.item = item
            itembox.pack_start(item, expand=True, fill=True, padding=0)
            # A box for the text that describes the functionality and any additional content.
            contentbox = Gtk.VBox(spacing=4)
            label = Gtk.Label(descriptions[i])
            label.set_xalign(0)
            label.set_line_wrap(True)
            contentbox.pack_start(label, expand=True, fill=True, padding=0)
            # Main box for each item.
            mainbox = Gtk.Box()
            mainbox.itembox = itembox
            mainbox.contentbox = contentbox
            mainbox.pack_start(itembox, expand=False, fill=False, padding=0)
            mainbox.pack_start(contentbox, expand=False, fill=False, padding=0)
            itemboxes.append(mainbox)
        return itemboxes

class DeviceSync(SettingsSection):
    """This section deals with how PyFit will update its data from a GadgetBridge database."""

    #### Properties ##############

    @property
    def settings(self):
        settings = {}
        settings['gbpath'] = self.subsections['Methods']['entries']['gbpath'].get_text()
        settings['scriptpath'] = self.subsections['Methods']['entries']['scriptpath'].get_text()
        if self.subsections['Methods']['checkboxes']['script'].itembox.item.get_active():
            settings['gbmethod'] = 'runscript'
        else:
            settings['gbmethod'] = 'readfile'
        settings['gbremovefile'] = self.subsections['Methods']['checkboxes']['gbremovefile'].itembox.item.get_active()
        options = ['Refresh', 'Overwrite']
        settings['gbimportmode'] = options[int(self.subsections['Behavior']['radiobuttons']['Overwrite'].itembox.item.get_active())]
        return settings

    #### Initialization ##########

    def __init__(self):
        self.subsections = {'Methods': _g('Update Mode'), 'Behavior': _g('Import behavior')}
        super().__init__()

        '''The basic elements of the subsections are already initialized by the parent 
        class (box and title), but further elements need to be added.'''

        #######################
        ### Methods section ###
        #######################
        '''The methods section specifies mainly where to find the GadgetBridge database 
        file to update the data and gives the option tu run a script or program that 
        fetches the database from the phone automatically.'''
        self.subsections['Methods']['entries'] = {}
        self.subsections['Methods']['checkboxes'] = {}

        #### OPTION: GadgetBridge's database path.
        box = Gtk.VBox()
        #- Label
        label = Gtk.Label(_g('Database location'))
        label.set_xalign(0)
        label.set_line_wrap(True)
        #- File chooser button.
        button = Gtk.Button()
        Image = Gtk.Image.new_from_icon_name('gtk-directory', self.iconsize)
        button.set_image(Image)
        button.set_always_show_image(True)
        button.set_relief(Gtk.ReliefStyle(2))
        button.connect('clicked', self.on_file_clicked, 'Methods', 'gbpath')
        #- Text entry.
        entry = Gtk.Entry()
        #- Generate the layout of the option.
        entrybox = Gtk.Box()
        entrybox.pack_start(entry, expand=False, fill=False, padding=0)
        entrybox.pack_start(button, expand=False, fill=False, padding=0)
        box.pack_start(label, expand=False, fill=False, padding=0)
        box.pack_start(entrybox, expand=False, fill=False, padding=0)
        #- Attach the relevant items to the subsection dictionary
        self.subsections['Methods']['entries']['gbpath'] = entry
        self.subsections['Methods']['box'].pack_start(box, expand=False, fill=False, padding=0)

        #### OPTIONS: Run script and/or delete file after importing.
        #- Create both checkboxes.
        descriptions = (_g('Launch the following program before importing the database:'),
                        _g('Delete the source file after a successful import.'))
        checkboxes = self.creategroup('Checkbox', None, descriptions)
        names = ('script', 'gbremovefile')
        for checkbox, name in ( (checkboxes[i], names[i]) for i in range(0, len(descriptions)) ):
            self.subsections['Methods']['box'].pack_start(checkbox, expand=False, fill=False, padding=0)
            self.subsections['Methods']['checkboxes'][name] = checkbox
        #- Launch script option specific code.
        #-- Text entry.
        entry = Gtk.Entry()
        #- File chooser button.
        button = Gtk.Button()
        Image = Gtk.Image.new_from_icon_name('gtk-directory', self.iconsize)
        button.set_image(Image)
        button.set_always_show_image(True)
        button.set_relief(Gtk.ReliefStyle(2))
        button.connect('clicked', self.on_file_clicked, 'Methods','scriptpath')
        #- Generate the layout of the option.
        entrybox = Gtk.Box()
        entrybox.pack_start(entry, expand=False, fill=False, padding=0)
        entrybox.pack_start(button, expand=False, fill=False, padding=0)
        self.subsections['Methods']['checkboxes']['script'].contentbox.pack_start(entrybox, expand=False, fill=False, padding=0)
        #- Attach the relevant items to the subsection dictionary
        self.subsections['Methods']['entries']['scriptpath'] = entry

        #######################
        ### Behavior section ##
        #######################
        '''The behavior section specifies whether all the previous activities and datapoints 
        proceeding from GadgetBridge should be deleted and replaced by the new ones when 
        refreshing the data or not.'''
        self.subsections['Behavior']['radiobuttons'] = {}

        # Further construction of the behavior section.
        descriptions = (_g('Refresh'), _g('Overwrite'))
        radioboxes = self.creategroup('Radio', None, descriptions)
        for i in range(0, len(radioboxes)):
            self.subsections['Behavior']['box'].pack_start(radioboxes[i], expand=False, fill=False, padding=0)
            self.subsections['Behavior']['radiobuttons'][descriptions[i]] = radioboxes[i]

        self.show_all()

    #### Event handlers ##########

    def update_gui(self):
        settings = self.get_toplevel().settings
        self.subsections['Methods']['checkboxes']['script'].itembox.item.set_active(settings['gbmethod'] == 'runscript')
        self.subsections['Methods']['checkboxes']['gbremovefile'].itembox.item.set_active(settings['gbremovefile'])
        for entry in ['gbpath', 'scriptpath']:
            text = settings[entry]
            if not text:
                text = ''
            self.subsections['Methods']['entries'][entry].set_text(text)
        for button in ['Refresh', 'Overwrite']:
            self.subsections['Behavior']['radiobuttons'][button].itembox.item.set_active(settings['gbimportmode'] == button)

    #### Action handlers ##########

    def on_file_clicked(self, button, subsection, name):
        dialog = Gtk.FileChooserDialog(_g("Please choose a file"), button.get_toplevel(),
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.subsections[subsection]['entries'][name].set_text(dialog.get_filename())
        dialog.destroy()

class Goals(SettingsSection):
    """This section deals let's the user configure a set of fitness goals to be reached."""

    #### Properties ##############

    @property
    def settings(self):
        settings = {}
        # TODO: Implement different time frames for the goals.
        # settings['goaltimeframe'] = self.subsections['Goals']['comboboxes']['timeframe'].get_active_text()
        settings['goaltimeframe'] = 'Daily'
        for goal in ['distancegoal', 'stepsgoal', 'minutesgoal']:
            settings[goal] = self.subsections['Goals']['spinbuttons'][goal].get_value()
            settings[goal + 'enabled'] = self.subsections['Goals']['checkboxes'][goal].itembox.item.get_active()
        for setting in ['distancegoal', 'stepsgoal', 'minutesgoal']:
            if settings[setting] <= 0:
                settings[setting] = None
        return settings

    #### Initialization ##########

    def __init__(self):
        self.subsections = {'Goals': _g('Goals')}
        super().__init__()

        '''The basic elements of the subsections are already initialized by the parent 
        class (box and title), but further elements need to be added.'''

        #######################
        #### Goals section ####
        #######################
        self.subsections['Goals']['comboboxes'] = {}
        self.subsections['Goals']['checkboxes'] = {}
        self.subsections['Goals']['spinbuttons'] = {}

        # TODO: Implement different time frames for the goals.
        #### OPTION: Timeframe for the goals.
        # box = Gtk.Box()
        # #- Label
        # label = Gtk.Label('Timeframe:')
        # label.set_xalign(0)
        # label.set_line_wrap(True)
        # box.pack_start(label, expand=False, fill=False, padding=0)
        # # - Combobox.
        # combobox = Gtk.ComboBoxText()
        # for frame in ['Daily', 'Weekly']:
        #     combobox.append_text(frame)
        # self.subsections['Goals']['comboboxes']['timeframe'] = combobox
        # box.pack_start(combobox, expand=False, fill=False, padding=0)
        # self.subsections['Goals']['box'].pack_start(box, expand=False, fill=False, padding=0)

        #### OPTION: Checkboxes and spinbuttons for each goal.
        # - Create checkboxes with spinbutton for each goal.
        descriptions = (_g('Distance goal'),
                        _g('Steps goal'),
                        _g('Minutes goal'))
        names = ('distancegoal', 'stepsgoal', 'minutesgoal')
        checkboxes = self.creategroup('Checkbox', None, descriptions)
        for checkbox, name in ( (checkboxes[i], names[i]) for i in range(0, len(descriptions)) ):
            self.subsections['Goals']['box'].pack_start(checkbox, expand=False, fill=False, padding=0)
            self.subsections['Goals']['checkboxes'][name] = checkbox
            self.subsections['Goals']['checkboxes'][name].itembox.item.connect('toggled', self.on_goalstatuschange)
        # Distance spinbutton.
        spinbutton = Gtk.SpinButton().new_with_range(0, 50000, 0.1)
        spinbutton.set_orientation(Gtk.Orientation.HORIZONTAL)
        spinbutton.wrap = True
        spinbutton.snap_to_ticks = True
        self.subsections['Goals']['spinbuttons']['distancegoal'] = spinbutton
        self.subsections['Goals']['checkboxes']['distancegoal'].contentbox.pack_start(spinbutton, expand=False, fill=False, padding=0)
        # Steps goal spinbutton.
        spinbutton = Gtk.SpinButton().new_with_range(0, 200000, 1)
        spinbutton.set_orientation(Gtk.Orientation.HORIZONTAL)
        spinbutton.wrap = True
        spinbutton.snap_to_ticks = True
        self.subsections['Goals']['spinbuttons']['stepsgoal'] = spinbutton
        self.subsections['Goals']['checkboxes']['stepsgoal'].contentbox.pack_start(spinbutton, expand=False, fill=False, padding=0)
        # Minutes goal spinbutton.
        spinbutton = Gtk.SpinButton().new_with_range(0, 99999, 1)
        spinbutton.set_orientation(Gtk.Orientation.HORIZONTAL)
        spinbutton.wrap = True
        spinbutton.snap_to_ticks = True
        self.subsections['Goals']['spinbuttons']['minutesgoal'] = spinbutton
        self.subsections['Goals']['checkboxes']['minutesgoal'].contentbox.pack_start(spinbutton, expand=False, fill=False, padding=0)


    #### Event handlers ##########

    def update_gui(self):
        if not hasattr(self, 'firstguiupdate'):
            self.firstguiupdate = True
        if self.firstguiupdate == True:
            self.firstguiupdate = False
            settings = self.get_toplevel().settings
            # TODO: Implement different time frames for the goals.
            # if settings['goaltimeframe'] == 'Daily':
            #     self.subsections['Goals']['comboboxes']['timeframe'].set_active(0)
            # elif settings['goaltimeframe'] == 'Weekly':
            #     self.subsections['Goals']['comboboxes']['timeframe'].set_active(1)
            for goal in ['distancegoal','stepsgoal','minutesgoal']:
                if settings[goal] != None:
                    self.subsections['Goals']['spinbuttons'][goal].set_value(settings[goal])
                if settings[goal + 'enabled'] and settings[goal] != None and settings[goal] > 0:
                    self.subsections['Goals']['checkboxes'][goal].itembox.item.set_active(True)
                else:
                    self.subsections['Goals']['checkboxes'][goal].itembox.item.set_active(False)
        else:
            self.firstguiupdate = False
            settings = self.settings
        for goal in ['distancegoal', 'stepsgoal', 'minutesgoal']:
            if settings[goal + 'enabled']:
                self.subsections['Goals']['spinbuttons'][goal].set_sensitive(True)
            else:
                self.subsections['Goals']['spinbuttons'][goal].set_sensitive(False)

    #### Action handlers ##########

    def on_goalstatuschange(self, button):
        self.update_gui()
