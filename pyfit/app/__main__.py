'''This file contains the entry entry point for the program.'''

##### PyFit modules. #####
from libs import importexport as ie
from libs import platform
from libs import data
from app import settings as st, views as vw
####### PyGobject ########
import gi
from libs.platform import gtkversion, pyfiticon
gi.require_version('Gtk', gtkversion)
from gi.repository import Gtk, GdkPixbuf
###### Time and date #####
import datetime as dt
######### Others #########
import threading
import notify2
import gettext
_g = gettext.gettext

class MainWindow(Gtk.Window):
    '''This class is the entry point for the program. It creates and populates a window with all the GUI elements.'''

    def __init__(self):
        super().__init__()

        #### Build the main window and the base Layout.
        self.basegrid = Gtk.Grid()
        self.basegrid.set_margin_top(20)
        self.basegrid.set_margin_bottom(20)
        self.basegrid.set_margin_start(12)
        self.basegrid.set_margin_end(12)
        self.basegrid.set_hexpand(True)
        self.basegrid.set_vexpand(True)
        self.add(self.basegrid)

        #--- Menu
        self.menu = Gtk.VBox(spacing=0)
        self.menu.set_margin_top(5)
        self.menu.set_halign(Gtk.Align.CENTER)
        self.menu.set_valign(Gtk.Align.FILL)
        self.menu.set_vexpand(True)
        self.menu.set_hexpand(False)
        self.basegrid.attach(self.menu, 0, 0, 1, 1)

        #--- Views area
        self.viewsgrid = Gtk.Grid()
        self.viewsgrid.set_column_spacing(50)
        self.viewsgrid.set_row_spacing(30)
        self.viewsgrid.set_hexpand(True)
        self.viewsgrid.set_vexpand(True)
        self.viewsgrid.set_halign(Gtk.Align.FILL)
        self.viewsgrid.set_valign(Gtk.Align.FILL)
        self.viewsgrid.set_margin_start(25)
        self.basegrid.attach(self.viewsgrid, 1, 0, 1, 1)

        # Create and init the main window, the views container and button container.
        self.views = {}
        self.buttons = {}

        #### Set up the left area, with overall information, controls and settings.

        #--- Get the user's picture and name.
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(platform.userpicture)
        pixbuf = pixbuf.scale_simple(100, 100, GdkPixbuf.InterpType.BILINEAR)
        Image = Gtk.Image.new_from_pixbuf(pixbuf)
        Image.set_valign(Gtk.Align.START)
        self.menu.pack_start(Image, expand=False, fill=True, padding=0)
        label = Gtk.Label(label=platform.username)
        label.set_line_wrap(True)
        label.set_max_width_chars(13)
        label.set_width_chars(13)
        label.set_valign(Gtk.Align.START)
        self.menu.pack_start(label, expand=False, fill=True, padding=2)

        #--- Create the settings and refresh buttons.
        iconsize = 2

        # Settings button.
        self.buttons['Settings'] = Gtk.Button(label='Settings')
        Image = Gtk.Image.new_from_icon_name('gtk-properties', iconsize)
        self.buttons['Settings'].set_image(Image)
        self.buttons['Settings'].set_always_show_image(True)
        self.buttons['Settings'].connect("clicked", self.on_Settings_clicked)
        self.menu.pack_start(self.buttons['Settings'], expand=False, fill=False, padding=2)
        self.buttons['Settings'].set_valign(Gtk.Align.START)

        # Refresh button.
        self.buttons['Refresh'] = Gtk.Button(label='Refresh')
        Image = Gtk.Image.new_from_icon_name('gtk-refresh', iconsize)
        self.buttons['Refresh'].set_image(Image)
        self.buttons['Refresh'].set_always_show_image(True)
        self.buttons['Refresh'].connect("clicked", self.on_Refresh_clicked)
        self.menu.pack_start(self.buttons['Refresh'], expand=False, fill=False, padding=2)
        self.buttons['Refresh'].set_valign(Gtk.Align.START)

        #--- Create the goals view.
        self.views['Goals'] = vw.GoalsView(date=dt.date.today())
        self.views['Goals'].upperbox.set_vexpand(False)
        self.views['Goals'].lowerbox.set_vexpand(False)
        self.views['Goals'].infobox.set_vexpand(False)
        self.views['Goals'].set_valign(Gtk.Align.START)
        self.views['Goals'].set_halign(Gtk.Align.FILL)
        self.menu.pack_start(self.views['Goals'], expand=False, fill=False, padding=40)

        #### Set up the right area, that shows the user's data.

        views = {'Journal': [_g('Journal'), vw.JournalView(date=dt.date.today()), (0,0)],
                 'Sleep':  [_g('Sleep'), vw.SleepView(date=dt.date.today()), (0,1)],
                 'HeartRate':  [_g('Heart Rate'), vw.HeartRateView(date=dt.date.today()), (1,0)],
                 'Weight':  [_g('Weight'), vw.WeightView(date=dt.date.today()), (1,1)]
                 }

        for key, item in views.items():
            label = Gtk.Label()
            label.set_markup('<b>{}</b>'.format(item[0]))
            box = Gtk.VBox(spacing=5)
            box.set_hexpand(True)
            box.set_vexpand(True)
            self.views[key] = item[1]
            box.pack_start(label, expand=False, fill=False, padding=0)
            box.pack_start(self.views[key], expand=True, fill=True, padding=0)
            self.viewsgrid.attach(box, item[2][0], item[2][1], 1, 1)

        #--- Set the minimum size for the views and add reference to the main window in them.
        for key, view in [ (key, view) for key, view in self.views.items() if key != 'Goals' ]:
            view.set_size_request(390,200)
            view.infobox.set_size_request(140, 180)
            view.GUI = self

        # Set the window name.
        self.set_role('PyFit')
        self.set_icon_from_file(pyfiticon)
        self.set_title ("PyFit")
        self.set_wmclass("PyFit", "PyFit")  # TODO: This is deprecated.

        # Show the window and start the program.
        self.connect("destroy", Gtk.main_quit)
        self.show_all()
        Gtk.main()

    #### Action handlers ###

    def on_Settings_clicked(self, button):
            # Show the settings dialog and create a copy of the new settings in memory.
            dialog = st.SettingsScreen(self)
            settings = dialog.settings
            dialog.run()
            newsettings = dialog.settings
            dialog.destroy()

            # Use the new settings to find out whether to update the GUI or not.
            for setting in ['distancegoal','distancegoalenabled','stepsgoal', 'stepsgoalenabled',
                            'minutesgoal', 'minutesgoalenabled']:
                if settings[setting] != newsettings[setting]:
                    self.views['Goals'].update()
                    break

    def on_Refresh_clicked(self, button):
        # TODO: Replace the icon with a spinner and rotate it while the data is loading.
        exitcode, message = ie.gbdataupdate()
        # The exit code is True whenever the update was successful and False when it is not.
        if exitcode == True:
            for key, view in self.views.items():
                view.update()
            # TODO: Change the button color to green and then fade it to normal after a while.
            # Show a notification when the process is complete. In order to make the notification disappear after a
            # while, the threading module is used.
            n = notify2.Notification(_g('Success'), message, 'gtk-ok')
            n.show()
            close_thread = threading.Timer(4.9, n.close)
            close_thread.start()
        else:
            # TODO: Change the button color to red and then fade it to normal after a while.
            # In this case the notification remains forever.
            n = notify2.Notification(_g('Error updating the database'), message, 'gtk-dialog-warning')
            n.show()
        # TODO: Implement a notification view as a sort of "top" bar that shows the messages instead of using the system's
        # notifications, or give the user a choice.

if __name__ == '__main__':
    notify2.init('PyFit')
    data.init_database()
    data.init_settings()
    gui = MainWindow()
